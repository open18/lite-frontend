import { Container, Grid, TextField } from "@mui/material";
import React from "react";
import { Controller, useForm } from "react-hook-form";
import { useDispatch, useSelector } from "react-redux";
import { startLogin } from "../store/auth/thunks";
import LoadingButton from "@mui/lab/LoadingButton";

const Login = (props) => {
  const dispatch = useDispatch();
  const { control, handleSubmit } = useForm();
  const { loading } = useSelector((state) => state.auth);

  const onSubmit = (data) => {
    const { email, password } = data;
    if ((email, password)) {
      dispatch(startLogin({ email, password }));
    }
  };

  return (
    <Container maxWidth="sm">
      <form onSubmit={handleSubmit(onSubmit)}>
        <Grid container spacing={2}>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <h2>Login page</h2>
          </Grid>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <Controller
              name="email"
              control={control}
              render={({ field }) => (
                <TextField
                  fullWidth
                  {...field}
                  label="Email"
                  type="email"
                  required
                />
              )}
            />
          </Grid>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <Controller
              name="password"
              control={control}
              render={({ field }) => (
                <TextField
                  fullWidth
                  {...field}
                  label="Password"
                  type="password"
                  required
                />
              )}
            />
          </Grid>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <LoadingButton variant="contained" type="submit" loading={loading}>
              Login
            </LoadingButton>
          </Grid>
        </Grid>
      </form>
    </Container>
  );
};

export default Login;
