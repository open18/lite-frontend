import React, { useEffect } from "react";
import MaterialTable from "@material-table/core";
import { Container } from "@mui/system";
import { Grid, TextField } from "@mui/material";
import { useDispatch, useSelector } from "react-redux";
import { Controller, useForm } from "react-hook-form";
import { createCompany, fetchListCompanies } from "../store/companies";
import RefreshIcon from "@mui/icons-material/Refresh";
import LoadingButton from "@mui/lab/LoadingButton";
import { toast } from "react-toastify";
import { mapDate } from "../functions/dateUtil";

import { useNavigate } from "react-router-dom";
import { useUser } from "../hooks/useUser";

import RPI from "react-phone-input-2";
// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
const PhoneInput = RPI.default ? RPI.default : RPI;
import "react-phone-input-2/lib/style.css";
import CompanyService from "../services/CompanyService";

const isPhoneValid = (phoneForValidate) => {
  return phoneForValidate && phoneForValidate.length > 5;
};

const Companies = (props) => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { control, handleSubmit, reset } = useForm();
  const { listCompanies, loading } = useSelector((state) => state.company);

  const { isAdmin } = useUser();

  const onSubmit = async (data) => {
    const { name, address, nit, phone } = data;
    if ((name, address, nit, phone)) {
      if (isPhoneValid(phone)) {
        await dispatch(createCompany({ name, address, nit, phone }));
        reset({
          name: "",
          address: "",
          nit: "",
          phone: "",
        });
      } else {
        toast.info("Phone is not valid!");
      }
    } else {
      toast.info("All data is necessary");
    }
  };

  const doFetchListCompanies = () => dispatch(fetchListCompanies());

  useEffect(() => {
    doFetchListCompanies();
  }, []);

  const onRowUpdate = async (newData, oldData) => {
    const response = await CompanyService.updateCompany({
      name: newData.name,
      address: newData.address,
      phone: newData.phone,
      idCompany: oldData._id,
    });
    if (response?.data?.message === "OK") {
      doFetchListCompanies();
      toast.success("Updated!");
    } else {
      toast.error("Failed");
    }
  };

  const onRowDelete = async (oldData) => {
    const response = await CompanyService.deleteCompany(oldData._id);
    if (response?.data?.message === "OK") {
      doFetchListCompanies();
      toast.success("Deleted!");
    } else {
      toast.error("Failed");
    }
  };

  return (
    <div style={{ paddingTop: 50 }}>
      {isAdmin && (
        <Container>
          <form onSubmit={handleSubmit(onSubmit)}>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <span>New Company</span>
              </Grid>
              <Grid item xs={12} sm={3} md={3} lg={3}>
                <Controller
                  name="nit"
                  control={control}
                  render={({ field }) => (
                    <TextField
                      fullWidth
                      {...field}
                      label="Nit"
                      type="text"
                      required
                    />
                  )}
                />
              </Grid>
              <Grid item xs={12} sm={3} md={3} lg={3}>
                <Controller
                  name="name"
                  control={control}
                  render={({ field }) => (
                    <TextField
                      fullWidth
                      {...field}
                      label="Name"
                      type="text"
                      required
                    />
                  )}
                />
              </Grid>
              <Grid item xs={12} sm={3} md={3} lg={3}>
                <Controller
                  name="address"
                  control={control}
                  render={({ field }) => (
                    <TextField
                      fullWidth
                      {...field}
                      label="Address"
                      type="text"
                      required
                    />
                  )}
                />
              </Grid>
              <Grid item xs={12} sm={3} md={3} lg={3}>
                <Controller
                  name="phone"
                  control={control}
                  //defaultValue={organizer.urlPlayStore}
                  // render={({ field }) => <Input {...field} type="cel" required fullWidth />}
                  render={({ field }) => (
                    <PhoneInput
                      required
                      country={"co"}
                      {...field}
                      inputStyle={{ width: "100%", height: 50 }}
                    />
                  )}
                />
              </Grid>
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <LoadingButton
                  variant="contained"
                  type="submit"
                  loading={loading}
                >
                  Save
                </LoadingButton>
              </Grid>
            </Grid>
          </form>
        </Container>
      )}

      <div style={{ paddingTop: 50 }}>
        <MaterialTable
          onRowClick={(e, rowData) => {
            if (isAdmin) navigate(`/dashboard/company/${rowData._id}`);
          }}
          title={"Companies"}
          columns={[
            { title: "Nit", field: "nit", editable: "never" },
            { title: "Name", field: "name" },
            { title: "Address", field: "address" },
            { title: "Phone", field: "phone", type: "numeric" },
            { title: "Created at", field: "created_at", editable: "never" },
          ]}
          data={listCompanies.map((item) => ({
            ...item,
            created_at: mapDate(item.created_at),
          }))}
          isLoading={loading}
          actions={[
            {
              icon: () => <RefreshIcon />,
              tooltip: "Refresh",
              isFreeAction: true,
              onClick: doFetchListCompanies,
            },
          ]}
          options={{
            actionsColumnIndex: -1,
            pageSize: 20,
          }}
          editable={
            isAdmin
              ? {
                  onRowUpdate: onRowUpdate,
                  onRowDelete: onRowDelete,
                }
              : {}
          }
        />
      </div>
    </div>
  );
};

export default Companies;
