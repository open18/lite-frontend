import React, { useEffect, useState } from "react";
import MaterialTable from "@material-table/core";
import RefreshIcon from "@mui/icons-material/Refresh";
import { useNavigate, useParams } from "react-router-dom";
import { fetchCompanyStock } from "../store/companies/thunks";
import { useDispatch, useSelector } from "react-redux";
import { mapDate } from "../functions/dateUtil";
import { ExportPdf } from "@material-table/exporters";
import moment from "moment";
import { Container } from "@mui/system";
import { Grid, TextField } from "@mui/material";
import { LoadingButton } from "@mui/lab";
import CompanyService from "../services/CompanyService";
import { toast } from "react-toastify";
import GoBack from "../components/GoBack";
import { Controller, useForm } from "react-hook-form";

const CompanyStock = (props) => {
  const { id: idCompany } = useParams();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const { control, handleSubmit, reset } = useForm();

  const { listCompanyStock, loading: storeLoading } = useSelector(
    (state) => state.company
  );

  const doFetchListCompanyStock = () => dispatch(fetchCompanyStock(idCompany));

  const [fileHelper, setFileHelper] = useState(null);
  const [internalLoading, setInternalLoading] = useState(false);

  useEffect(() => {
    doFetchListCompanyStock();
  }, [idCompany]);

  const handleSubmitEmail = async (e) => {
    e.preventDefault();
    if (fileHelper && e.target.email.value) {
      setInternalLoading(true);
      const response = await CompanyService.sendEmail({
        file: fileHelper,
        emailTo: e.target.email.value,
      });
      if (response?.data?.message === "OK") {
        if (e.target.reset) e.target.reset();
        setFileHelper(null);
        toast.success("Mail sent");
      } else {
        toast.error("Failed");
      }
      setInternalLoading(false);
    }
  };

  const onRowUpdate = async (newData, oldData) => {
    const response = await CompanyService.updateCompanyStock({
      name: newData.name,
      amount: newData.amount,
      idCompanyStock: oldData._id,
    });
    if (response?.data?.message === "OK") {
      doFetchListCompanyStock();
      toast.success("Updated!");
    } else {
      toast.error("Failed");
    }
  };

  const onRowDelete = async (oldData) => {
    const response = await CompanyService.deleteCompanyStock(oldData._id);
    if (response?.data?.message === "OK") {
      doFetchListCompanyStock();
      toast.success("Deleted!");
    } else {
      toast.error("Failed");
    }
  };

  const onSubmit = async (data) => {
    const { name, amount } = data;
    if ((name, amount)) {
      setInternalLoading(true);
      const response = await CompanyService.createCompanyStock({
        name,
        amount,
        idCompany,
      });
      if (response?.data?.message === "OK") {
        reset({
          name: "",
          amount: "",
        });
        toast.success("Created!");
        doFetchListCompanyStock();
      } else {
        toast.error("Failed");
      }
      setInternalLoading(false);
    } else {
      toast.info("All data is necessary");
    }
  };

  const loading = storeLoading || internalLoading;

  return (
    <div style={{ paddingTop: 50 }}>
      <Grid container>
        <GoBack back={() => navigate("/dashboard")} />
        <Grid item xs={12} md={12} lg={12} sm={12}>
          <form onSubmit={handleSubmit(onSubmit)}>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <span>New item</span>
              </Grid>
              <Grid item xs={12} sm={3} md={3} lg={3}>
                <Controller
                  name="name"
                  control={control}
                  render={({ field }) => (
                    <TextField
                      fullWidth
                      {...field}
                      label="Name"
                      type="text"
                      required
                    />
                  )}
                />
              </Grid>
              <Grid item xs={12} sm={3} md={3} lg={3}>
                <Controller
                  name="amount"
                  control={control}
                  render={({ field }) => (
                    <TextField
                      fullWidth
                      {...field}
                      label="Amount"
                      type="number"
                      required
                    />
                  )}
                />
              </Grid>

              <Grid item xs={12} sm={12} md={12} lg={12}>
                <LoadingButton
                  variant="contained"
                  type="submit"
                  loading={loading}
                >
                  Save
                </LoadingButton>
              </Grid>
            </Grid>
          </form>
        </Grid>
        <Grid item xs={12} md={12} lg={12} sm={12}>
          <MaterialTable
            title={"Items"}
            columns={[
              { title: "Name", field: "name" },
              { title: "Amount", field: "amount", type: "numeric" },
              { title: "Created at", field: "created_at", editable: "never" },
            ]}
            data={listCompanyStock.map((item) => ({
              ...item,
              created_at: mapDate(item.created_at),
            }))}
            isLoading={loading}
            actions={[
              {
                icon: () => <RefreshIcon />,
                tooltip: "Refresh",
                isFreeAction: true,
                onClick: doFetchListCompanyStock,
              },
            ]}
            options={{
              actionsColumnIndex: -1,
              pageSize: 5,
              exportMenu: [
                {
                  label: "Export PDF",
                  //// You can do whatever you wish in this function. We provide the
                  //// raw table columns and table data for you to modify, if needed.
                  // exportFunc: (cols, datas) => console.log({ cols, datas })
                  exportFunc: (cols, datas) =>
                    ExportPdf(
                      cols,
                      datas,
                      `Stock_PDF_${moment().format("DD_MMMM_YY_hh_mm_A")}`
                    ),
                },
              ],
            }}
            editable={{
              onRowUpdate: onRowUpdate,
              onRowDelete: onRowDelete,
            }}
          />
        </Grid>
      </Grid>

      <Container>
        <form method="post" onSubmit={handleSubmitEmail}>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <h3>Send file by email</h3>
            </Grid>
            <Grid item xs={12} sm={6} md={6} lg={6}>
              <input
                type="file"
                name="file"
                required
                onChange={(e) => setFileHelper(e.target.files[0])}
              />
            </Grid>
            <Grid item xs={12} sm={6} md={6} lg={6}>
              <TextField
                fullWidth
                label="Email"
                type="email"
                name="email"
                required
              />
            </Grid>
            <Grid item xs={12} sm={12} md={12} lg={12}>
              <LoadingButton
                variant="contained"
                type="submit"
                loading={loading}
              >
                Send
              </LoadingButton>
            </Grid>
          </Grid>
        </form>
      </Container>
      <br />
      <br />
      <br />
      <br />
      <br />
    </div>
  );
};

export default CompanyStock;
