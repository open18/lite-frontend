import { useSelector } from "react-redux";

export const useUser = () => {
  const { user } = useSelector((state) => state.auth);

  const isAdmin = user?.role === "ADMIN";
  return {
    isAdmin,
  };
};
