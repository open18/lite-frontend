import rootApi from "../api/rootApi";

export default class CompanyService {
  static async sentRequest(config) {
    try {
      const res = await rootApi(config);
      return res;
    } catch (error) {
      console.error(error.message);
      return null;
    }
  }

  static async sendEmail({ file, emailTo }) {
    const data = new FormData();
    data.append("file", file);
    data.append("emailTo", emailTo);
    console.log("data:", data);

    const config = {
      method: "post",
      url: `company-stock/send-email`,
      headers: {
        "Content-Type": "multipart/form-data",
      },
      data,
    };

    return CompanyService.sentRequest(config);
  }

  static async updateCompanyStock(params) {
    const config = {
      method: "put",
      url: `company-stock`,
      headers: {
        "Content-Type": "application/json",
      },
      data: JSON.stringify(params),
    };

    return CompanyService.sentRequest(config);
  }

  static async deleteCompanyStock(id) {
    const config = {
      method: "delete",
      url: `company-stock/${id}`,
    };

    return CompanyService.sentRequest(config);
  }

  static async updateCompany(params) {
    const config = {
      method: "put",
      url: `company`,
      headers: {
        "Content-Type": "application/json",
      },
      data: JSON.stringify(params),
    };

    return CompanyService.sentRequest(config);
  }

  static async deleteCompany(id) {
    console.log("444");
    const config = {
      method: "delete",
      url: `company/${id}`,
    };

    return CompanyService.sentRequest(config);
  }

  static async createCompanyStock(params) {
    const config = {
      method: "post",
      url: `company-stock`,
      headers: {
        "Content-Type": "application/json",
      },
      data: JSON.stringify(params),
    };

    return CompanyService.sentRequest(config);
  }
}
