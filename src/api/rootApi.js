import axios from "axios";
const baseURL = import.meta.env.VITE_LITE_API;

const rootApi = axios.create({
  baseURL,
  headers: {
    "Content-Type": "application/json",
  },
  // withCredentials: true,
});

rootApi.interceptors.request.use(async (config) => {
  const token = await sessionStorage.getItem("x-token");

  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }
  return config;
});

export default rootApi;
