import moment from "moment";

const format = "DD MMM YYYY, hh:mm A";

export function mapDate(object) {
  try {
    return moment(object).format(format);
  } catch (error) {
    console.error("mapDate: ", error.message);
  }
  return "";
}
