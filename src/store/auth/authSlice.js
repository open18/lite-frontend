import { createSlice } from "@reduxjs/toolkit";

const initUser = () => {
  try {
    const user = JSON.parse(sessionStorage.getItem("user"));
    return user;
  } catch (error) {
    return {};
  }
};

const initialState = {
  user: initUser(),
  loading: false,
};

export const authSlice = createSlice({
  name: "auth",
  initialState,
  reducers: {
    setLoading: (state, { payload }) => {
      state.loading = payload;
    },
    login: (state, { payload }) => {
      state.user = payload;
    },
  },
});

export const { login, setLoading } = authSlice.actions;
