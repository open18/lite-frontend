import { toast } from "react-toastify";
import rootApi from "../../api/rootApi";
import { login, setLoading } from "./authSlice";

export const startLogin = ({ email, password }) => {
  return async (dispatch) => {
    try {
      dispatch(setLoading(true));
      const response = await rootApi.post("/auth/login", {
        username: email,
        password,
      });

      if (response?.data?.access_token) {
        sessionStorage.setItem("x-token", response?.data?.access_token);
        sessionStorage.setItem("user", JSON.stringify(response?.data?.user));

        dispatch(login(response?.data?.user));
      }
    } catch (error) {
      toast.error("Wrong credentials!");
      console.log(error);
    } finally {
      dispatch(setLoading(false));
    }
  };
};

export const startLogout = () => {
  return async (dispatch) => {
    sessionStorage.clear();
    dispatch(login(null));
  };
};
