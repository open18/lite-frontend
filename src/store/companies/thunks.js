import { toast } from "react-toastify";
import rootApi from "../../api/rootApi";
import {
  setListCompanies,
  setListCompanyStock,
  setLoading,
} from "./companySlice";

export const fetchListCompanies = () => {
  return async (dispatch) => {
    try {
      dispatch(setLoading(true));
      const response = await rootApi.get("/company");

      if (response?.data?.message === "OK") {
        dispatch(setListCompanies(response?.data?.data));
      }
    } catch (error) {
      toast.error("Error!");
      console.log(error);
    } finally {
      dispatch(setLoading(false));
    }
  };
};

export const createCompany = ({ name, address, nit, phone }) => {
  return async (dispatch) => {
    try {
      dispatch(setLoading(true));
      const response = await rootApi.post("/company", {
        name,
        address,
        nit,
        phone,
      });

      if (response?.data?.message === "OK") {
        toast.success("Company created");
        dispatch(fetchListCompanies());
      }
    } catch (error) {
      if (error?.response?.data?.message === "COMPANY_EXISTS") {
        toast.error("There is a company with this NIT");
      } else {
        toast.error("Error!");
      }
    } finally {
      dispatch(setLoading(false));
    }
  };
};

export const fetchCompanyStock = (idCompany) => {
  return async (dispatch) => {
    try {
      dispatch(setLoading(true));
      const response = await rootApi.get(
        `/company-stock/idCompany/${idCompany}`
      );

      if (response?.data?.message === "OK") {
        dispatch(setListCompanyStock(response?.data?.data));
      }
    } catch (error) {
      toast.error("Error!");
      console.log(error);
    } finally {
      dispatch(setLoading(false));
    }
  };
};
