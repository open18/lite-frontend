import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  listCompanies: [],
  listCompanyStock: [],
  loading: false,
};

export const companySlice = createSlice({
  name: "company",
  initialState,
  reducers: {
    setLoading: (state, { payload }) => {
      state.loading = payload;
    },
    setListCompanies: (state, { payload }) => {
      state.listCompanies = payload;
    },
    setListCompanyStock: (state, { payload }) => {
      state.listCompanyStock = payload;
    },
  },
});

export const { setLoading, setListCompanies, setListCompanyStock } =
  companySlice.actions;
