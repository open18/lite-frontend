import { configureStore } from "@reduxjs/toolkit";
import { authSlice } from "./auth/authSlice";
import { companySlice } from "./companies";

export const store = configureStore({
  reducer: {
    auth: authSlice.reducer,
    company: companySlice.reducer,
  },
});
